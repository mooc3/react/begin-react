import monstera from '../assets/monstera.jpg'

export const plantList = [
	{
		name: 'monstera',
		category: 'classique',
		id: '1ed',
		isBestSale: true,
		light: 2,
		water: 3,
        cover: monstera,
		price: 12
	},
	{
		name: 'ficus lyrata',
		category: 'classique',
		id: '2ab',
		light: 3,
		water: 1,
        cover: monstera,
		price: 13.5
	},
	{
		name: 'pothos argenté',
		category: 'classique',
		id: '3sd',
        isSpecialOffer: true,
		light: 1,
		water: 2,
        cover: monstera,
		price: 17
	},
	{
		name: 'yucca',
		category: 'classique',
		id: '4kk',
		light: 3,
		water: 1,
        cover: monstera,
		price: 14.50
	},
	{
		name: 'olivier',
		category: 'extérieur',
		id: '5pl',
		light: 3,
		water: 1,
        cover: monstera,
		price: 25
	},
	{
		name: 'géranium',
		category: 'extérieur',
		id: '6uo',
		light: 2,
		water: 2,
        cover: monstera,
		price: 13
	},
	{
		name: 'basilique',
		category: 'extérieur',
		id: '7ie',
        isSpecialOffer: true,
		isBestSale: true,
		light: 2,
		water: 3,
        cover: monstera,
		price: 6
	},
	{
		name: 'aloe',
		category: 'plante grasse',
		id: '8fp',
		light: 2,
		water: 1,
        cover: monstera,
		price: 12
	},
	{
		name: 'succulente',
		category: 'plante grasse',
		id: '9vn',
        isSpecialOffer: true,
		light: 2,
		water: 1,
        cover: monstera,
		price: 8
	}
]