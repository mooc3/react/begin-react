import '../styles/Banner.css'
import logo from '../assets/logo.png'
import Recommendations from './Recommendations'

function Banner () {
    const title = "La Maison Jungle"
    return (
        <header className="lmj-banner">
            <div className="lmj-logoContainer">
                <img src={logo} alt="La Maison Jungle" className="lmj-logo" />
                <h1 className="lmj-title">{title}</h1>
            </div>
            <Recommendations />
        </header>
    );
}

export default Banner;