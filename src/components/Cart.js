import '../styles/Cart.css'
import {useState, useEffect} from 'react'

function Cart ({cart, updateCart}) {
    const [isOpen, setIsOpen] = useState(true)
    const total = cart.reduce((acc, item) => acc + item.price * item.amount, 0)

    useEffect(() => {
        document.title = `Montant panier: ${total}€`
    }, [total])

    function handleAddOneItem (index, item) {
        const newCart = cart
        if (newCart[index].name === item.name) {
            newCart[index].amount += 1
            updateCart([...cart])
        }
    }

    function handleDelOneItem (index, item) {
        const newCart = cart
        if (newCart[index].amount - 1 > 0 && newCart[index].name === item.name) {
            newCart[index].amount -= 1
            updateCart([...newCart])
        } else  if (newCart[index].name === item.name) {
            newCart.splice(index, 1)
            updateCart([...newCart])
        }
    }

    return isOpen ? (
        <div className='lmj-cart'>
            <button onClick={() => setIsOpen(false)} className='lmj-cart-toggle-button'>Fermer mon panier</button>
            <h2>Mon panier</h2>
            <ul className='lmj-cart-list-item'>
            {cart.map((item, index) => 
                <li key={`${item.name}`} className='lmj-cart-item'>
                    <div className='lmj-cart-item-text-container'>
                        <p>{item.name}:</p>
                        <p>{item.price * item.amount}€</p>
                    </div>
                    <div className='lmj-cart-item-button-container'>
                        <button 
                            onClick={() => handleDelOneItem(index, item)}
                            className='lmj-cart-item-button lmj-cart-item-button-mod' >
                                -
                        </button>
                        {item.amount}
                        <button
                            onClick={() => handleAddOneItem(index, item)}
                            className='lmj-cart-item-button lmj-cart-item-button-mod'>
                                +
                        </button>
                        <button
                            onClick={() => {updateCart(cart.filter(itm => itm !== item))}}
                            className='lmj-cart-item-button lmj-cart-item-button-del'>
                                Supprimer
                        </button>
                    </div>
                </li>
            )}
            </ul>
            {cart.length === 0 ? (
                <p>Votre panier est vide</p>
            ) : (
                <div>
                    <p>Total: {total}€</p>
                    <button onClick={() => updateCart([])} className='lmj-cart-clean-button'>Vider le panier</button>
                </div>
            )}
        </div>
    ) : (
        <div className='lmj-cart-closed'>
            <button onClick={() => setIsOpen(true)} className='lmj-cart-toggle-button'>Ouvrir mon panier</button>
        </div>
    )
}

export default Cart