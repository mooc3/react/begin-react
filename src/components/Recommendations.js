import '../styles/Recommendations.css'

function seasonReco () {
    const currentMonth = new Date().getMonth()

    switch (currentMonth) {
        case 2:
        case 3:
        case 4:
            return "C'est le printemps, pensez à rempoter 🌱"
        case 5:
        case 6:
        case 7:
            return "C'est l'été, pensez à arroser 🕶️"
        case 8:
        case 9:
        case 10:
            return "C'est l'autonme, pensez à planter 🍂"
        case 11:
        case 0:
        case 1:
            return "C'est l'hiver, pensez à rentrer vos plantes ❄️"
        default:
            break
    }
}

function Recommendations () {
    return (
        <div className="lmj-recommendations">
            <p>{seasonReco()}</p>
        </div>
    )
}

export default Recommendations