import '../styles/PlantItem.css'
import CareScale from './CareScale'

function PlantItem ({name, price, cover, light, water, isSpecialOffer, cart, updateCart}) {
    function addToCart (name, price) {
        const currentPlantAdded = cart.find((plant) => plant.name === name)
        if (currentPlantAdded) {
            const currentCartAdded = cart.filter((plant) => plant.name !== currentPlantAdded.name)
            sortByNameAndUpdate([...currentCartAdded, {name, price, amount: currentPlantAdded.amount + 1}])
        } else {
            let newAdded = {name, price, 'amount': 1}
            sortByNameAndUpdate([...cart, newAdded])
        }
    }

    function sortByNameAndUpdate (tab) {
        tab.sort((a, b) => a.name > b.name)
        updateCart(tab)
    }

    return (
        <li className='lmj-plant-container'>
            <div className='lmj-plant-item'>
                {isSpecialOffer && <span className='lmj-sales'>Soldes</span>}
                <span className='lmj-plant-item-price'>{price}€</span>
                <img className="lmj-plant-item-cover" src={cover} alt={name} />
                {name}
                <div>
                    <CareScale scaleValue={light} careType="light" />
                    <CareScale scaleValue={water} careType="water" />
                </div>
                <button onClick={() => addToCart(name, price)} className='lmj-plant-item-add'>Ajouter au panier</button>
            </div>
        </li>
    )
}

export default PlantItem