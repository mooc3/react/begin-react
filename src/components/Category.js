import '../styles/Category.css'

function Category ({category, setCategory, catList}) {
    return (
        <div className="lmj-plant-category">
            <select value={category} onChange={(e) => setCategory(e.target.value)} className="lmj-plant-category-select">
                <option value=''></option>
                {catList.map((cat, index) => (
                    <option value={cat} key={`${cat}-${index}`}>{cat}</option>
                ))}
            </select>
            <button onClick={(e) => setCategory('')}>Réinitialiser</button>
        </div>
    )
}

export default Category