import '../styles/CareScale.css'
import Sun from '../assets/sun.svg'
import Water from '../assets/water.svg'

function CareScale ({scaleValue, careType}) {
    const range = [1, 2, 3]
    const scaleType = careType === 'light'
        ? <img className="lmj-careScale-icon" src={Sun} alt="Sun icon" />
        : <img className="lmj-careScale-icon" src={Water} alt="Water icon" />

    function careInform (careType, scaleValue) {
        const recoScale = {
            1: "peu",
            2: "modérément",
            3: "beaucoup"
        }
        alert(`Cette plante requière ${recoScale[scaleValue]} ${careType === 'light' ? "de lumière" : "d'arrosage"}`)
    }
    
    return (
        <div className="lmj-careScale">
            <div>
                {range.map((elem) => 
                    scaleValue >= elem ? <span key={elem}>{scaleType}</span> : null
                )}
            </div>
            <button className="lmj-careScale-btn" onClick={() => careInform(careType, scaleValue)}>?</button>
        </div>
    )
}

export default CareScale