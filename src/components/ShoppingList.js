import '../styles/ShoppingList.css'
import {plantList} from '../datas/plantList'
import PlantItem from './PlantItem'
import Category from './Category'
import {useState} from 'react'

function ShoppingList ({cart, updateCart}) {
    const [activeCategory, setCategory] = useState('')
    const catList = plantList.reduce(
		(acc, plant) =>
			acc.includes(plant.category) ? acc : acc.concat(plant.category),
		[]
	)
    return (
        <div className="lmj-plant">
            <Category
                category={activeCategory}
                setCategory={setCategory}
                catList={catList}
            />
            <ul className='lmj-plant-list'>
                {plantList.map(({name, id, price, cover, light, water, isSpecialOffer, category}) => (
                    activeCategory === category || activeCategory === '' ?
                    (<PlantItem 
                        key={id}
                        name={name}
                        price={price}
                        cover={cover}
                        light={light}
                        water={water}
                        isSpecialOffer={isSpecialOffer}
                        cart={cart}
                        updateCart={updateCart}
                    />) : (null)
                ))}
            </ul>
        </div>
    )
}

export default ShoppingList